﻿using UnityEngine;

public class CheckWall : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("ParedeEsquerda"))
        {
            gameObject.GetComponentInParent<PlayerMovement>().isOnLeftWall = true;
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("ParedeDireita"))
        {
            gameObject.GetComponentInParent<PlayerMovement>().isOnRightWall = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("ParedeEsquerda"))
        {
            gameObject.GetComponentInParent<PlayerMovement>().isOnLeftWall = false;
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("ParedeDireita"))
        {
            gameObject.GetComponentInParent<PlayerMovement>().isOnRightWall = false;
        }
    }
}
