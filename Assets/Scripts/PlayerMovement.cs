﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D myRB;
    
    public float jumpVelocity = 70;
    public float initialVerticalVelocity = 5;
    public float minimumVerticalVelocity = 2;
    public float maximumVerticalVelocity = 10;
    public float accelerationThroughLevel = 0.5f;
    public float accelerationThroughInput = 0.25f;

    public float verticalSpeed { get; private set; }
    private readonly float xSpeed;
    private float move;

    [HideInInspector]
    public bool isOnLeftWall;
    [HideInInspector]
    public bool isOnRightWall;
    
    private bool jumpRightTrigger = false;
    private bool jumpLeftTrigger = false;
    private bool accelerateRequested = false;
    private bool deccelerateRequested = false;

    public GameManager gameManager;
    protected Joystick mobileJoystick;
    private bool noAccelerationRequested;
    [HideInInspector]
    public bool initialized = false;
    
    void Start()
    {
        myRB = GetComponent<Rigidbody2D>();

        isOnLeftWall = true;
        isOnRightWall = false;

        verticalSpeed = initialVerticalVelocity;

        mobileJoystick = FindObjectOfType<Joystick>();

        initialized = true;
    }

    void Update()
    {
        HandleInputsFromJoystick();
    }

    private void HandleInputsFromJoystick()
    {
        var horizontalAxis = Input.GetAxis("Horizontal") + (mobileJoystick.Horizontal);
        var verticalAxis = Input.GetAxis("Vertical") + (mobileJoystick.Vertical);

        CheckIfJumpRequested(horizontalAxis);
        CheckIfAccelerationRequested(verticalAxis);
    }

    private void CheckIfAccelerationRequested(float verticalAxis)
    {
        if (verticalAxis > 0.1)
        {
            accelerateRequested = true;
        }
        else if (verticalAxis < -0.1)
        {
            deccelerateRequested = true;
        }
    }

    private void CheckIfJumpRequested(float horizontalAxis)
    {
        if (horizontalAxis > 0.1 && !jumpRightTrigger)
        {
            jumpRightTrigger = true;
        }
        else if (horizontalAxis < -0.1 && !jumpLeftTrigger)
        {
            jumpLeftTrigger = true;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        HandleLeftAndRightJumps();

        UpdateInertialVelocityWithAccelerationThroughLevelTime();

        HandleVerticalAccelerationThroughInput();

        CapMaximumAndMinimumVerticalSpeed();

        myRB.velocity = new Vector2(myRB.velocity.x, verticalSpeed);
    }

    private void HandleLeftAndRightJumps()
    {
        if (jumpRightTrigger)
        {
            jumpRightTrigger = false;
            if (isOnLeftWall && !isOnRightWall)
            {
                myRB.velocity = new Vector2(jumpVelocity, myRB.velocity.y);
            }
        }
        if (jumpLeftTrigger && !isOnLeftWall)
        {
            jumpLeftTrigger = false;
            if (isOnRightWall)
            {
                myRB.velocity = new Vector2(-jumpVelocity, myRB.velocity.y);
            }
        }
    }

    private void UpdateInertialVelocityWithAccelerationThroughLevelTime()
    {
        verticalSpeed += Time.fixedDeltaTime * accelerationThroughLevel;
        initialVerticalVelocity += Time.fixedDeltaTime * accelerationThroughLevel;
    }

    private void HandleVerticalAccelerationThroughInput()
    {
        if (accelerateRequested)
        {
            accelerateRequested = false;
            verticalSpeed += accelerationThroughInput;
        }
        else if (deccelerateRequested)
        {
            deccelerateRequested = false;
            verticalSpeed -= accelerationThroughInput;
        }
        else
        {
            if (verticalSpeed > initialVerticalVelocity)
            {
                verticalSpeed -= accelerationThroughInput;
            }
            else if (verticalSpeed < initialVerticalVelocity)
            {
                verticalSpeed += accelerationThroughInput;
            }
        }
    }

    private void CapMaximumAndMinimumVerticalSpeed()
    {
        if (verticalSpeed > maximumVerticalVelocity)
        {
            verticalSpeed = maximumVerticalVelocity;
        }
        else if (verticalSpeed < minimumVerticalVelocity)
        {
            verticalSpeed = minimumVerticalVelocity;
        }
    }

    public void Die()
    {
        gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        Destroy(gameObject);
        gameManager.RestartTheGameAfterSomeTime();
    }

    public void Win()
    {
        gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        Destroy(gameObject);
        gameManager.Win();
    }
}
