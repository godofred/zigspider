﻿using UnityEngine;

public class KillPlayerOnTouch : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Character"))
        {
            other.gameObject.GetComponent<PlayerMovement>().Die();
        }
    }
}