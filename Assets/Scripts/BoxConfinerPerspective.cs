﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

[ExecuteInEditMode]
public class BoxConfinerPerspective : MonoBehaviour {

    PolygonCollider2D[] colliderList;
    PolygonCollider2D visibleCollider, realCollider;
    GameObject currentVirtualCam;
    GameObject brainCam;
    CinemachineVirtualCamera currentVirtualCamComponent;
    int n;
    float diffHeight, diffWidth;
    int collisionIndex;
    CinemachineConfiner cineConf;

    // Use this for initialization
    void Awake () {
        brainCam = GameObject.Find("Main Camera");
    }

    void Start()
    {
        currentVirtualCam = brainCam.GetComponent<CinemachineBrain>().ActiveVirtualCamera.VirtualCameraGameObject;

        currentVirtualCamComponent = currentVirtualCam.GetComponent<CinemachineVirtualCamera>();

        diffHeight = GetDiffPerspective(Mathf.Abs(currentVirtualCam.transform.position.z), currentVirtualCamComponent.m_Lens.FieldOfView);
        diffWidth = diffHeight * currentVirtualCamComponent.m_Lens.Aspect;

        cineConf = currentVirtualCam.GetComponent<CinemachineConfiner>();
        colliderList = GetComponents<PolygonCollider2D>();
        visibleCollider = colliderList[1];
        realCollider = colliderList[0];
    }

    // Update is called once per frame
    void Update()
    {
        realCollider.points = new[] { new Vector2(visibleCollider.points[0].x + diffWidth, Mathf.Clamp(visibleCollider.points[0].y - diffHeight,0, visibleCollider.points[0].y - diffHeight)), new Vector2(visibleCollider.points[1].x + diffWidth, Mathf.Clamp(visibleCollider.points[1].y + diffHeight, visibleCollider.points[1].y + diffHeight,0)), new Vector2(visibleCollider.points[2].x - diffWidth, Mathf.Clamp(visibleCollider.points[2].y + diffHeight, visibleCollider.points[2].y + diffHeight,0)), new Vector2(visibleCollider.points[3].x - diffWidth, Mathf.Clamp(visibleCollider.points[3].y - diffHeight, 0, visibleCollider.points[3].y - diffHeight)) };

        cineConf = currentVirtualCam.GetComponent<CinemachineConfiner>();
        cineConf.InvalidatePathCache();
    }

    void FixedUpdate()
    {
        currentVirtualCam = brainCam.GetComponent<CinemachineBrain>().ActiveVirtualCamera.VirtualCameraGameObject;
        
        currentVirtualCamComponent = currentVirtualCam.GetComponent<CinemachineVirtualCamera>();
        diffHeight = GetDiffPerspective(Mathf.Abs(currentVirtualCam.transform.position.z), currentVirtualCamComponent.m_Lens.FieldOfView);
        diffWidth = diffHeight * currentVirtualCamComponent.m_Lens.Aspect;
    }

    float GetDiffPerspective(float cameraDistance, float FOV_deg)
    {
        return cameraDistance * Mathf.Tan(FOV_deg * 0.5f * Mathf.Deg2Rad);
    }
}
