﻿using UnityEngine;

public class WaveMovement : MonoBehaviour
{
    private Rigidbody2D myRB;

    public float initialVerticalVelocity = 6;
    public float accelerationThroughLevel = 0.6f;
    private float verticalSpeed;
    
    void Start()
    {
        myRB = GetComponent<Rigidbody2D>();
        verticalSpeed = initialVerticalVelocity;
    }
    
    void FixedUpdate()
    {
        verticalSpeed += Time.fixedDeltaTime * accelerationThroughLevel;
        myRB.velocity = new Vector2(myRB.velocity.x, verticalSpeed);
    }
}
