﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public float timeToWaitBeforeRestart = 3;
    [SerializeField]
    private GameObject winText;

    private bool restartTrigger = false;
    private float resetTime;
    private TimeManager timeManager;
    private ScoreManager scoreManager;

    private void Start()
    {
        timeManager = FindObjectOfType<TimeManager>();
        scoreManager = FindObjectOfType<ScoreManager>();
    }

    void Update()
    {
        if (restartTrigger && resetTime <= Time.time)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    public void RestartTheGameAfterSomeTime()
    {
        timeManager.enabled = false;
        restartTrigger = true;
        resetTime = Time.time + timeToWaitBeforeRestart;
    }

    public void Win()
    {
        winText.SetActive(true);
        scoreManager.AddToScore(Mathf.FloorToInt(timeManager.timeLeft) * 5);
        RestartTheGameAfterSomeTime();
    }
}
