﻿using System;
using UnityEngine;

namespace Assets.Scripts.Dtos
{
    [Serializable]
    public class MinutesAndSeconds
    {
        [SerializeField]
        public int Minutes = 3;
        [SerializeField, Range(0, 59)]
        public int Seconds = 0;
    }
}
