﻿using UnityEngine;
using Cinemachine;

/// <summary>
/// An add-on module for Cinemachine Virtual Camera that locks the camera's Z co-ordinate
/// </summary>
[ExecuteInEditMode]
[SaveDuringPlay]
[AddComponentMenu("")] // Hide in menu
public class CinemachineLockAxis : CinemachineExtension
{
    [Tooltip("Lock the camera's X position to this value")]
    public bool LockXPosition = true;
    public float m_XPosition = 0f;
    public bool LockYPosition = false;
    public float m_YPosition = 0f;
    public bool LockZPosition = false;
    public float m_ZPosition = 0f;

    protected override void PostPipelineStageCallback(
        CinemachineVirtualCameraBase vcam,
        CinemachineCore.Stage stage, ref CameraState state, float deltaTime)
    {
        if (stage == CinemachineCore.Stage.Body)
        {
            var pos = state.RawPosition;
            if (LockXPosition)
                pos.x = m_XPosition;
            if(LockYPosition)
                pos.y = m_YPosition;
            if (LockZPosition)
                pos.z = m_ZPosition;
            state.RawPosition = pos;
        }
    }
}