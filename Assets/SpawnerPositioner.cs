﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode()]
public class SpawnerPositioner : MonoBehaviour
{
    public float DistanceFromCameraOnXAxis = 0;
    public float DistanceFromCameraOnYAxis = 0;

    CinemachineBrain brain;
    GameObject currentVirtualCam;
    CinemachineVirtualCamera currentVirtualCamComponent;
    float viewSizeXAxis, viewSizeYAxis;
    GameObject mainCam;

    private bool initialized = false;

    private CinemachineVirtualCamera ActiveVirtualCamera
    {
        get { return brain == null ? null : brain.ActiveVirtualCamera as CinemachineVirtualCamera; }
    }

    void Awake()
    {
        mainCam = GameObject.Find("MainCamera");
    }

    void Start()
    {
        brain = mainCam.GetComponent<CinemachineBrain>();
    }

#if UNITY_EDITOR
    void Update()
    {
        if (!EditorApplication.isPlaying)
        {
            currentVirtualCamComponent = ActiveVirtualCamera;
            if (currentVirtualCamComponent != null)
            {
                currentVirtualCam = currentVirtualCamComponent.gameObject;

                viewSizeYAxis = currentVirtualCamComponent.m_Lens.OrthographicSize;
                viewSizeXAxis = viewSizeYAxis * currentVirtualCamComponent.m_Lens.Aspect;
                initialized = true;
            }
            if (initialized)
            {
                transform.position = new Vector3(currentVirtualCam.transform.position.x + viewSizeXAxis + DistanceFromCameraOnXAxis, currentVirtualCam.transform.position.y + viewSizeYAxis + DistanceFromCameraOnYAxis, transform.position.z);
            }
        }
    }

    private void FixedUpdate()
    {
        if (EditorApplication.isPlaying)
        {
            currentVirtualCamComponent = ActiveVirtualCamera;
            if (currentVirtualCamComponent != null)
            {
                currentVirtualCam = currentVirtualCamComponent.gameObject;

                viewSizeYAxis = currentVirtualCamComponent.m_Lens.OrthographicSize;
                viewSizeXAxis = viewSizeYAxis * currentVirtualCamComponent.m_Lens.Aspect;
                initialized = true;
            }
            if (initialized)
            {
                transform.position = new Vector3(currentVirtualCam.transform.position.x + viewSizeXAxis + DistanceFromCameraOnXAxis, currentVirtualCam.transform.position.y + viewSizeYAxis + DistanceFromCameraOnYAxis, transform.position.z);
            }
        }
    }
#endif

#if !UNITY_EDITOR
    private void FixedUpdate()
    {
        currentVirtualCamComponent = ActiveVirtualCamera;
        if (currentVirtualCamComponent != null)
        {
            currentVirtualCam = currentVirtualCamComponent.gameObject;

            viewSizeYAxis = currentVirtualCamComponent.m_Lens.OrthographicSize;
            viewSizeXAxis = viewSizeYAxis * currentVirtualCamComponent.m_Lens.Aspect;
            initialized = true;
        }
        if (initialized)
        {
            transform.position = new Vector3(currentVirtualCam.transform.position.x + viewSizeXAxis + DistanceFromCameraOnXAxis, currentVirtualCam.transform.position.y + viewSizeYAxis + DistanceFromCameraOnYAxis, transform.position.z);
        }
    }
#endif
}
