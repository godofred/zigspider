﻿using UnityEngine;

public class DestroyObjectsOnTouch : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Enemy") || other.gameObject.layer == LayerMask.NameToLayer("Collectable"))
        {
            Destroy(other.gameObject);
        }
    }
}