﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    public List<GameObject> spawnObject;
    public float maxTime = 5;
    public float minTime = 2;

    [SerializeField]
    private float width = 0;
    [SerializeField]
    private float height = 0;

    private float timeSinceLastSpawn;
    private float nextSpawnTime;
    private GameObject nextObjectTypeToSpawn;

    void Start()
    {
        SetRandomNextSpawnTime();
        timeSinceLastSpawn = 0;
    }

    void FixedUpdate()
    {
        timeSinceLastSpawn += Time.deltaTime;
        if (timeSinceLastSpawn >= nextSpawnTime)
        {
            PickNextRandomSpawnObjectToInstantiate();
            SpawnObjectInsideBox();
            SetRandomNextSpawnTime();
            timeSinceLastSpawn = 0;
        }
    }

    void SetRandomNextSpawnTime()
    {
        nextSpawnTime = Random.Range(minTime, maxTime);
    }

    void SpawnObjectInsideBox()
    {
        Vector3 randomPositionInsideBox = new Vector3(gameObject.transform.position.x + Random.Range(-width / 2, width / 2), gameObject.transform.position.y + Random.Range(-height / 2, height / 2), gameObject.transform.position.z);
        Instantiate(nextObjectTypeToSpawn, randomPositionInsideBox, nextObjectTypeToSpawn.transform.rotation);
    }

    void PickNextRandomSpawnObjectToInstantiate()
    {
        nextObjectTypeToSpawn = spawnObject[Random.Range(0, spawnObject.Count)];
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        float wHalf = (width * .5f);
        float hHalf = (height * .5f);
        Vector3 topLeftCorner = new Vector3(transform.position.x - wHalf, transform.position.y + hHalf, 1f);
        Vector3 topRightCorner = new Vector3(transform.position.x + wHalf, transform.position.y + hHalf, 1f);
        Vector3 bottomLeftCorner = new Vector3(transform.position.x - wHalf, transform.position.y - hHalf, 1f);
        Vector3 bottomRightCorner = new Vector3(transform.position.x + wHalf, transform.position.y - hHalf, 1f);
        Gizmos.DrawLine(topLeftCorner, topRightCorner);
        Gizmos.DrawLine(topRightCorner, bottomRightCorner);
        Gizmos.DrawLine(bottomRightCorner, bottomLeftCorner);
        Gizmos.DrawLine(bottomLeftCorner, topLeftCorner);
    }
}
