﻿using UnityEngine;

public class IncreaseScoreOnTouch : MonoBehaviour
{
    public int ScoreToAdd = 10;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Character"))
        {
            Destroy(gameObject);
            FindObjectOfType<ScoreManager>().AddToScore(ScoreToAdd);
        }
    }
}
