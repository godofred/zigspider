﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Text scoreValueUIElement;
    public int Score { get; private set; }

    void Start()
    {
        Score = 0;
        scoreValueUIElement = gameObject.GetComponent<Text>();
        UpdateScoreTextUI();
    }

    private void UpdateScoreTextUI()
    {
        scoreValueUIElement.text = Score.ToString();
    }

    public void AddToScore(int scoreToAdd)
    {
        Score += scoreToAdd;
        UpdateScoreTextUI();
    }
}
