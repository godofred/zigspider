﻿using Assets.Scripts.Dtos;
using UnityEngine;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour
{
    [SerializeField]
    private MinutesAndSeconds startTime;
    private Text timeLeftTextUI;
    [HideInInspector]
    public float timeLeft;

    [HideInInspector]
    public bool timerEnabled;

    void Start()
    {
        timeLeftTextUI = gameObject.GetComponent<Text>();
        timeLeft = startTime.Minutes * 60.0f + startTime.Seconds;
        timerEnabled = true;
    }

    void FixedUpdate()
    {
        if (timerEnabled)
        {
            UpdateTimeLeftTextUI();
        }
    }

    private void UpdateTimeLeftTextUI()
    {
        timeLeft -= Time.deltaTime;
        timeLeftTextUI.text = Float2TimeString(timeLeft);
    }

    string Float2TimeString(float timeInSeconds)
    {
        int minutes = Mathf.FloorToInt(timeInSeconds / 60F);
        int seconds = Mathf.FloorToInt(timeInSeconds - minutes * 60);
        return string.Format("{0:0}:{1:00}", minutes, seconds);
    }
}
