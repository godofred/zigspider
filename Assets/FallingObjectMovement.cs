﻿using UnityEngine;

public class FallingObjectMovement : MonoBehaviour
{
    private Rigidbody2D myRB;
    private PlayerMovement character;

    public float verticalDropVelocity = 10;

    void Start()
    {
        myRB = GetComponent<Rigidbody2D>();
    }

    private void Awake()
    {
        character = FindObjectOfType<PlayerMovement>();
    }

    private void FixedUpdate()
    {
        if (character != null && character.initialized)
        {
            myRB.velocity = new Vector2(myRB.velocity.x, -verticalDropVelocity + (-character.verticalSpeed));
        }
    }
}
